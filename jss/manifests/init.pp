class jss{
 firewall { '700 allow jss ssl':
    chain   => 'INPUT',
    state   => ['NEW'],
    dport   => '8443',
    proto   => 'tcp',
    action  => 'accept',
  }
 firewall { '800 allow jss plain text':
    chain   => 'INPUT',
    state   => ['NEW'],
    dport   => '8080',
    proto   => 'tcp',
    action  => 'accept',
  }

 service { 'jamf.tomcat8' :
	ensure => running,
	enable => true,
  }
if $facts['system_purpose'] == 'backend'{
  cron { 'backupJss' :
    ensure => present,
    command =>  '/usr/bin/java -jar /usr/local/jss/bin/JSSDatabaseUtil.jar backup -saveBackupTo /usr/local/jss/backups/database -deleteBackupsOlderThanDays 14',
    hour => 0,
    minute => 30,
    environment => ['MAILTO=jack.bishop@nih.gov'],
  }
}
}

