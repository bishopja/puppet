#!/bin/bash

#check for hostname and do the do
#yucca should get this one too
#sync from yucca, not jssext
#firewall from jssext not yucca

#!/bin/bash
synclog="/var/log/dpsync.log"
caspershare="/usr/local/jds/shares/CasperShare/ "
if [ -d "/usr/local/jds/shares/CasperShare/docs" ] ; then
  :
else
  `mount /usr/local/jds/shares/CasperShare`
fi
master_ds="/isilon/scss/CasperShare10/Packages/"
if [ -d $master_ds ] ; then
        echo "Everything looks good.  Starting sync now." >> $synclog
        rsync -rlptDv --delete $master_ds $caspershare >> $synclog
        chown -R apache:apache $caspershare
        chmod -R +r $caspershare
else
        echo "It looks like Isilon isn't mounted" >>$synclog
        echo "Mounting now..." >> $synclog
        mount nindsdirfs.ninds.nih.gov:/ifs/shares /isilon
fi
