##Class removeeset
#uninstalls and deletels esets from the machine

class removebes{
    service { 'besclient' :
              enable => false,
              ensure => 'stopped',
   }
    package { 'BESAgent' :
            ensure => 'purged',

    }
    file { '/opt/BESClient':
            ensure => 'absent',
            force => true,
        }
}
