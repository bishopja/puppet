# == Class: getpurpose
#
class getpurpose(
) {

  file { '/usr/local/bin/get_purpose.rb' :
    ensure  => 'file',
    owner   => 'root',
    group   => 'wheel',
    mode     => '700',
    content  => file('getpurpose/get_purpose.rb')
    }
    exec {'get_purpose':
       command => '/usr/local/bin/get_purpose.rb',
       require => File['/usr/local/bin/get_purpose.rb'],
       }
}
