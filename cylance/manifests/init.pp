##Installs Cylance agent and prerequesites
 
class cylance {
#if $facts['cylance'] == 'installed' {
#package { 'zlib':
#		provider => 'yum',
#		ensure => present,
#	}
#package { 'glibc.i686':
#                provider => 'yum',
#                ensure => present,
#}
#
#package { 'dbus-libs.i686':
#                provider => 'yum',
#                ensure => present,
#}
#if $facts['operatingsystemmajrelease'] == '6' {
#package { 'openssl.i686':
#                provider => 'yum',
#                ensure => present,
#}
#}
#package { 'libgcc.i686':
#                provider => 'yum',
#                ensure => present,
#}
#
#package { 'sqlite.i686':
#                provider => 'yum',
#                ensure => present,
#}
#
#package { 'openssl':
#                provider => 'yum',
#                ensure => present,
#}
#
#package { 'sqlite':
#                provider => 'yum',
#                ensure => present,
#}
#
#package { 'libgcc':
#                provider => 'yum',
#                ensure => present,
#}
#file { "/opt/cylance":
#	ensure => directory,
#	}
#file { "/opt/cylance/config_defaults.txt":
#	ensure => present,
#	content => file('cylance/config_defaults.txt'),
#	}
#file { "/usr/local/bin/get-cylance-logs":
#        ensure => present,
#        content => file("cylance/get-cylance-logs"),
#        owner => 'root',
#        group => 'wheel',
#        mode => '770',
#        }
#
exec { 'yum clean all':
	user => 'root',
	path => '/bin:/usr/bin',
	}

 package { 'CylancePROTECT':
    ensure => absent,
#	require => [ Package['zlib'], Package['glibc.i686'], Package['dbus-libs.i686'], Package['libgcc.i686'], Package['sqlite.i686'], Package['openssl'], Package['sqlite'], Package['libgcc'], File["/opt/cylance/config_defaults.txt"] ],
    }
#if $facts['operatingsystemmajrelease'] == '7' {
#	service { 'cylancesvc':
#		enable => true,
#		ensure => running,
#	}
#}
#}
}
