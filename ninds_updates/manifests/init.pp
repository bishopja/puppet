#== Class ninds_updates


#Remove the old update command
class ninds_updates{
   cron { 'oldUpdates':
	command => 'yum update -y --exclude="kernel*" | mail -s "`hostname` Yum Update Log" NINDS10ServerAdmins@ninds.nih.gov',
	ensure  => absent,
}
cron { 'newOldUpdates':
command => 'yum update -y --exclude="kernel*" | mail -s "`hostname` Yum Update Log" nindsdirserveradmins@ninds.nih.gov',
ensure  => absent,
}
cron { 'tbiOldUpdates':
command => 'yum update -y --exclude="kernel*" | mail -s "`hostname` Yum Update Log" NINDS10ServerAdmins@ninds.nih.gov,mitchellsc@mail.nih.gov',
ensure  => absent,
}
cron { 'tbiOldUpdates2':
command => '/usr/bin/yum update -y --exclude="kernel*" | mail -s "`hostname` Yum Update Log" NINDS10ServerAdmins@ninds.nih.gov,smitchell2@sapient.com,monye@sapient.com',
ensure  => absent,
}
file { '/usr/local/bin/cronic' :
 ensure  => 'present',
 content => file('ninds_updates/cronic'),
 mode    => '0755',
 }
 file { '/usr/local/bin/ninds_updates.sh' :
 ensure  => 'present',
 content => file ('ninds_updates/ninds_updates.sh'),
 mode    => '0755',
 }

  cron { 'updates':
    command   => '/usr/local/bin/cronic /usr/local/bin/ninds_updates.sh',
	   ensure    => present,
     minute    => fqdn_rand(60),
     hour      => '1',
     weekday   => '6',
     user       => 'root',
     environment => ['MAILTO=nindsdirserveradmins@ninds.nih.gov'],
     }
#
#
#
#
#
#
#
#
#
}

