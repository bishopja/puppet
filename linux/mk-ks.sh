##!/bin/bash


KS_TEMPLATE="ks_template.cfg"
BOOTFILE_TEMPLATE="bootfile_template"

BF_CFG_DIR="/tftpboot/pxelinux.cfg/"
KS_CFG_DIR="/tftpboot/"

usage(){
	echo "Usage: $0 -h [hostname] -m [MAC Address] -a [ipaddress] -t"
	echo "[hostname] = name of the server to be setup"
	echo "[MAC Address] = ethernet address of the primary NIC.  Must be in aa:aa:aa:aa:aa:aa format"
	echo "[ipaddress] must be ipv4 address"
	echo "-t is for testing mode.  All files will be read and written to the current directory ./"
	exit
}
while getopts ":h:a:m:t" opt; do
  case ${opt} in
    h ) 
    	HOSTNAME=${OPTARG}
    	HOSTNAME=$(tr "[:upper:]" "[:lower:]" <<< "${HOSTNAME}")
    	 ;;
    m )
    	MAC=${OPTARG}
    	if [[ ! "${MAC}" =~ ^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$ ]]; then
    		echo "It appears this isn't a valid MAC Address"
    	fi
    	;;
    a ) 
    	IP=${OPTARG}
    	;;
    t )
	KS_CFG_DIR="./"
	BF_CFG_DIR="./"
	;;
    : )
    	echo "Invalid option: ${OPTARG} requires an argument" 1>&2
    	;;
    \? ) 
    	usage
      ;;
  esac
done

case ${IP} in 
	165.112.112.*)
		SM="255.255.255.0"
		GW="165.112.112.1"
		;;
	156.40.18.*)
		SM="255.255.255.0"
		GW="156.40.18.1"
		;;
	*)
		echo "${IP} is an invalid ip address.  Please try again"
		exit
		;;
esac
#Added to compensate for the extra 0 by vmware
BOOTFILE=${BF_CFG_DIR}"01-"$(sed 's/:/-/g'<<<"${MAC}")
TARGET_OS="RHEL7"
#add rhel 6 support later with an options/argument

KS_CONF=${KS_CFG_DIR}${HOSTNAME}.cfg


#write bootfile
#replace the important information in the new file 
cat ${BOOTFILE_TEMPLATE}>${BOOTFILE}

$(sed -i "s/TARGET_OS/${TARGET_OS}/g" ${BOOTFILE})
$(sed -i "s/IP/${IP}/g" ${BOOTFILE})
$(sed -i "s/GW/${GW}/g" ${BOOTFILE})
$(sed -i "s/SM/${SM}/g" ${BOOTFILE})
$(sed -i "s/HOSTNAME/${HOSTNAME}/g" ${BOOTFILE})


#copy and write the kickstart file
cat ${KS_TEMPLATE}>${KS_CONF}

$(sed -i "s/TARGET_OS/${TARGET_OS}/g" ${KS_CONF})
$(sed -i "s/IP/${IP}/g" ${KS_CONF})
$(sed -i "s/GW/${GW}/g" ${KS_CONF})
$(sed -i "s/SM/${SM}/g" ${KS_CONF})
$(sed -i "s/HOSTNAME/${HOSTNAME}/g" ${KS_CONF})


