#!/bin/bash



MODULE_NAME=$1
MODULES_DIR="/etc/puppetlabs/code/environments/production/modules/"
MODULE_PATH=${MODULES_DIR}${MODULE_NAME}
if [[ -e ${MODULE_PATH} ]]; then
	/bin/echo "${MODULE_NAME} exists.  Moving along."
else
	/bin/echo "${MODULE_NAME} doesn't exist"
	/bin/echo "Creating directory structure and fixing permissions"
	/bin/mkdir ${MODULE_PATH}
	/usr/bin/setfacl -m u:git:rwx ${MODULE_PATH}
	/usr/bin/setfacl -m u:pe-puppet:rwx ${MODULE_PATH}
	#/bin/chown pe-puppet:pe-puppet ${MODULE_PATH}
	/bin/chmod 755 ${MODULE_PATH}
	/bin/echo "Everything looks good on the Puppet Master"
fi


