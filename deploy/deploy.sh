#!/bin/bash



MODULE_NAME=$1
MODULES_DIR="/etc/puppetlabs/code/environments/production/modules/"
MODULE_PATH=${MODULES_DIR}${MODULE_NAME}
LOCAL_PATH="../${MODULE_NAME}"
LOG="deploy.log"

if [ $# -eq 0 ]; then
	echo ""
	echo "Usage: $0 MODULE_NAME"
	echo " MODULE_NAME is the name of the folder one level up"
	echo ""
	echo "This script must be run from the deploy directory"
	echo "";
	exit 1;
fi
echo "Before you deploy ${MODULE_NAME}, please make sure it has been pushed to Bit Bucket."
sleep 5
read -p "Enter Y to continue:  " ANSWER
if [[ ${ANSWER} = 'y' ]] || [[ ${ANSWER} = 'Y' ]]; then
	echo "Good.  Moving on."
else
	exit 1
fi

 ssh -i private_key git@pm.ninds.nih.gov /local/git/deployPM.sh ${MODULE_NAME} >> ${LOG}
rsync -rlgov -e "ssh -i private_key" ${LOCAL_PATH}/ git@pm.ninds.nih.gov:${MODULE_PATH} --exclude=".git*">>${LOG}
 