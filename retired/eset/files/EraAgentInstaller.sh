#!/bin/sh -e
# ESET Remote Administrator (OnlineInstallerScript)
# Copyright (c) 1992-2016 ESET, spol. s r.o. All Rights Reserved

cleanup_file="$(mktemp -q)"
finalize()
{
  set +e
  if test -f "$cleanup_file"
  then
    while read f
    do
      unlink "$f"
    done < "$cleanup_file"
    unlink "$cleanup_file"
  fi
}

trap 'finalize' HUP INT QUIT TERM EXIT

eraa_server_hostname="nindsdirwinbind.ninds.nih.gov"
eraa_server_port="2222"
eraa_peer_cert_b64="MIIJhQIBAzCCCU8GCSqGSIb3DQEHAaCCCUAEggk8MIIJODCCA+8GCSqGSIb3DQEHBqCCA+AwggPcAgEAMIID1QYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIWnTWKs6LW0QCAggAgIIDqIe0oxoupyzxsRWDLTAGrLeWR993mUzz6pZHc5lSHzEUHR6ez/RVznmL1QEwaph9rh9qqWyLz8oMirh254K0Rb6ubMMoTP7RO8sAFc55gclMjvkrISHi5evXXNk10d/M86g8ANJ8mGBj4h7k31SwfsS+Ja2EJVUglNRtYvfqmJA9Sf7COHG0osDNGE23NG6QK7tVEdsz4Uj0ly4mrjl5scF/mybHN2CXAFyzE1sTboN3l4lgHfGnWhtE4WB8h87Y8L+QCH8WfW/a8Xc+G8uUVcWqCIbISpsqD4YL3YOswNVk8dSZI5HroRj2UxCOOY+ac+IELx0Xgo3wx+fnqw2qZVZOxoVbol7bttsjshlPJBobGpSgQqkWcPiW4khwx15DMSSZeHLoTVfucZcQ17eNTHp8cLQhy5Sq9qwyWCBLci+OysaviBOb7IkAKqJvSTgnZGsnRhZYG7ijW3PjshjukBiJeSTTv6q38ByiVGm5o4BmSR7BPiP121wQicG36Xp2h5hVioGtdWmaDx3MjZ+INxsgdddHsOL4z8EqrNrmNJlefPk+bc1/x4mn6NA1dgAeQhHH5BYpESVv3/MBPdD416UsMwA4HEwjD4U/3MTJsFYNicSWpGDM0+5d0SH0unDe4DDV533KpL0+RbCqPPJsya/ZiyHFzvPdmpcDow+ugALjHdRyKYd8bKeP7IeYBfoPOTKaMhupcspH4zeG1sED9P7FGIiANKyNZ2aMugHJ4JOIGg3fRJZotPzx23GIw335OImE0W865ZFgjbRpkBi/Qig4RDJF0YLoHjoy5CPNhs35/E+JTRtotQU6gR1hM6WvDHL+zOPWPF6oblqPdchS23mGhadhGgwAoAD+7kw9KhypzAbEnatfGpLPbZjRZWUM/uZSiiJvoiUi7uLlcxjTURQJcnzkzGJ0tI9pzJusSUwXhg42FC/AV8HpmJeFJbDFeyNa7rWVdcXv3X6t896xZEVJW2aNWWnLF0sg3TnpbRvZlo/A/IzxRTLRip2oFmEP8CJ0k0MJzjAsXKvaPRfCSOy/oLne5EMb8cVqcj2bgWw64oA2lyPDUPCZYqvM/+gAEoq3Aox8i8n0BrB02+rme8aamhyTpyXDZLjYvSIFP2ysGaIz0NUm5HOTUemSefvEE5BuXlSeBwIqw7oOCgg4Qviw6b/MuCV6LGVXGrSdC+BzqhAueIo9wKR8LDQ+/3HbiNF39+Xfe2tbnKu7lmcGOjs14hS+D/3d6TCCBUEGCSqGSIb3DQEHAaCCBTIEggUuMIIFKjCCBSYGCyqGSIb3DQEMCgECoIIE7jCCBOowHAYKKoZIhvcNAQwBAzAOBAgFPTHTLaOo8QICCAAEggTIlbkTc3fvewPfjzfPQyWcDFf7wDp6RdKri9B6vlHKaHzDlg86zT7zpfER7ergmIZksENexZFuG5zL0S4kFngn2uJbca9f8WRzWpDQ2uzS5wB35F2f88qvX3Ai3q+9XSXuRPHNLRspTwsJg3j95/A3FW0w6RHOkb6CJMorNd7iiBoCJOhdmo9yWJ2XQ566nUczBeD92+oo3uOXYMqX0yk1OBFIihuV3E/Be042013bcDpLW0dMu3C62v8A7qPs4mwZULjrVKn3/dtCb/zbiG+WkNaBCLqdv475Z9NF0hIPW6k5oj2eMqDheApi99q/Q2lsQCQG0tOJMdkPWskAUOwRDfbMZDjhYzcXsYUNR1UITqoWBBpgfvz/E6QT4P/nXzl8epOo/eqDGvH3D5uLLpC6ZY5R+8Grf07+0nnYbR7u+FMobOmql+05bv1BYsmjT1FGBqkbSQPfZXrsCYp+1+4FWWpq5I4zLYtL2BtcKbo7mw1vrAqd+PmKJPYS+BPKK0tAvuuh3mrsJRV82cyqjKCMjNtFZSdSJmvuHqONFc1KHaCm1pQiQ/Z3Nkm9SRKoYksO1gC+MDqzkzBh6Za+MkJQP0TxHnS10SWLIYR1kl7eX8SVVLDynor56avmN1Mn9dbBBQvFAn/3zj5ZasPDrYGxaiaHMl9VQOqlu9rmxNDeAqAXVdXkxoaOLpqaRRFVHg++WdKaVQz/Oj6sZlq9vqgYhO/Itd+A+bCYjfNvETzvle7OwOo9GiCCQ02w1GFRECDOdVB1g7pPdiN/NDLfu8ohFgpgpoSe1NqZzHhgM6o6tqX0VWVLTYNysQ72RSvTUxRZp0o7+d75r8EzuKrEJT4adj3H+J8WRgXPtTgj3H85skODfS7CRGk2gzIy6n7P8WQgRNI88nWwtXV31Z8/6pWWL1n1Kb4G2oxGg4EuYTJBi4Pkny2sOqI4V5ckbJBXdRk/xWJhSAnJL12tGjVNsTn6fcZonE1t80o73ypTAzarHp0q0RsV9VCYTzBtdj22FZapmGA9sd/u9BiqzPp+ClX5VDpcDJgqvOvEkRQAnlvDw6kCB584qNJjNyGr9RNhm67OyUZfl2QMJg0xCVsssEJ0Oxk701ahR04aF4G/6ZamIAfB8RBN05ZJ4niM5c7LmQpCbgTO1VB67PbR6PesTzmqcLtCUh45gwQR7OHIS/1T4ftDb6hgpIFXmT9h96xmMpual2/AxYWh8V6I3zmNjOtlxwaUow0B5R0IHmUGcS9KziHvt6AOLEhVrQtwXj4X6XlCUZBnLdaZq9VEg3PJarhuA6Sj23BpWGgIrck95+86bsqhCumZjbHnz1gLfNxa11KjP4T0AxQ5hJMtCNQ2r5PXTtLG30csMi0C8ddLolYyf8DvitNRaFXXbqkRoQB56xXxstLm+wkhFPqzwuo4bqOPXdUVGc1CiP/WkJVcw9u4wB1LSHbIQiTDCBfgg0xAk8zkNicSPI5n/g1O/Iqp9DElsD6AoT/02cmZ3jalW7YcokFkT34zKRuagkic++cfyBSAQtakwyicJZf8Ck2SMpR4nYgRARIbBuDB2WGVGysUM3E7oJiok8JvpHOADwgVRa9pnhbFsQxJ6kSkRaP6XmsBLLQs5rG7jGdxMSUwIwYJKoZIhvcNAQkVMRYEFNq513GUZxe9bPsaYD56HIwLn4KeMC0wITAJBgUrDgMCGgUABBSIp8Vt5wvnvnhGnOmkY3ChEyuGyAQIcYv/dMgMKfA="
eraa_peer_cert_pwd=""
eraa_ca_cert_b64="MIIDWzCCAkOgAwIBAgISAYNkgzuKikNqsjIFFpEHXFEBMA0GCSqGSIb3DQEBBQUAMDYxJzAlBgNVBAMMHlNlcnZlciBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTELMAkGA1UEBhMCVVMwHhcNMTYxMTA2MDUwMDAwWhcNMjYxMTA4MDUwMDAwWjA2MScwJQYDVQQDDB5TZXJ2ZXIgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA86/rZ9NdwgA8QcPD/uUsY9cnA6Zch0Ci68CsXJftAZa0Ib4b5ehdoij4wFYNPuayn/Bzn6AYTZO9xzmflPJdvVfqQM5QrBIk+2kY0smU1Q99KOk6+Hx4p8APiihasyZIlTojWOWokYqNEbnRnurvnxiIfK7VH1qcbGQBO6fWAiDaZFXoFngn6ZJM+HfvPxoQAm+B29nH7YXdmGcE4glERNmCPQfm+WRkyQYmseFzdCgTEbQ6SRbgCHgJB7agdXtGLIgf14AvMvmsRCdGgrFYy/h3S25yjKpbh3mnKr6+lupcONpkO0jG2B2kiUhuEUNtyzqzNo21XLPhsvgb0CThSQIDAQABo2MwYTAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQURPsolZsPqwAohwQxA//kcl7kRY0wHwYDVR0jBBgwFoAURPsolZsPqwAohwQxA//kcl7kRY0wDQYJKoZIhvcNAQEFBQADggEBAJjL64nytAzlcHhv6BBB1/n0F1VU6I6Y9ljYQa3t870n+rU8EHPxLsd+iJhg2Bobo5EOL00/xFw99bXf3BUPK03zKz17m3EX2mM75Oty7QPiaVkMwLSd2bbPiaLyvWEmW6RZMEg7gFnT45TNXRVQe3CThP9i/Xo7mkzYv3yTRJYxNXjUDVa/tj4ez+icfojceeTTHAr574tEFo5ePbwXbxgFOULulfyF/QA+z88jOFGFurE7YaxnNgpGzjicT27aCPZZ6lzJ7ip/sJ95LlehIwBUD38PrZAaqdJgjBMGkxqjlaV038a+UKDjDkmX15djcDd64qOwOUajah0gv0Tlq+4="
eraa_product_uuid=""
eraa_installer_url="http://repository.eset.com/v1/com/eset/apps/business/era/agent/v6/6.5.417.0/agent-linux-i386.sh"
eraa_installer_checksum="ff606fd0e4fb5f5de88bebf722667f85a0516ff5"
eraa_initial_sg_token=""

arch=$(uname -m)
if $(echo "$arch" | grep -E "^(x86_64|amd64)$" 2>&1 >> /dev/null)
then
    eraa_installer_url="http://repository.eset.com/v1/com/eset/apps/business/era/agent/v6/6.5.417.0/agent-linux-x86_64.sh"
    eraa_installer_checksum="e9b2f43f0fad23100eb0b5ea5c3ba6e14240aaae"
fi

if test -z $eraa_installer_url
then
  echo "No installer available for '$arch' arhitecture. Sorry :/"
  exit 1
fi

local_cert_path="$(mktemp -q -u)"
echo $eraa_peer_cert_b64 | base64 -d > "$local_cert_path" && echo "$local_cert_path" >> "$cleanup_file"

if test -n "$eraa_ca_cert_b64"
then
  local_ca_path="$(mktemp -q -u)"
  echo $eraa_ca_cert_b64 | base64 -d > "$local_ca_path" && echo "$local_ca_path" >> "$cleanup_file"
fi

local_installer="$(mktemp -q -u)"

eraa_http_proxy_value=""
if test -n "$eraa_http_proxy_value"
then
  export use_proxy=yes
  export http_proxy="$eraa_http_proxy_value"
  (wget --connect-timeout 300 --no-check-certificate -O "$local_installer" "$eraa_installer_url" || wget --connect-timeout 300 --no-proxy --no-check-certificate -O "$local_installer" "$eraa_installer_url" || curl --connect-timeout 300 -k "$eraa_installer_url" > "$local_installer") && echo "$local_installer" >> "$cleanup_file"
else
  (wget --connect-timeout 300 --no-check-certificate -O "$local_installer" "$eraa_installer_url" || curl --connect-timeout 300 -k "$eraa_installer_url" > "$local_installer") && echo "$local_installer" >> "$cleanup_file"
fi

echo -n "Checking integrity of installer script " && echo "$eraa_installer_checksum  $local_installer" | sha1sum -c

chmod +x "$local_installer"

local_migration_list="$(mktemp -q -u)"
tee "$local_migration_list" 2>&1 > /dev/null << __LOCAL_MIGRATION_LIST__

__LOCAL_MIGRATION_LIST__
test $? = 0 && echo "$local_migration_list" >> "$cleanup_file"

for dir in /sys/class/net/*/
do
    if test -f "$dir/address"
    then
        grep -E '00:00:00:00:00:00' "$dir/address" > /dev/null || macs="$macs $(sed 's/\://g' "$dir/address" | awk '{print toupper($0)}')"
    fi
done

while read line
do
    if test -n "$macs" -a -n "$line"
    then
        mac=$(echo $line | awk '{print $1}')
        uuid=$(echo $line | awk '{print $2}')
        lsid=$(echo $line | awk '{print $3}')
        if $(echo "$macs" | grep "$mac" > /dev/null)
        then
            if test -n "$mac" -a -n "$uuid" -a -n "$lsid"
            then
                additional_params="--product-guid $uuid --log-sequence-id $lsid"
                break
            fi
        fi
    fi
done < "$local_migration_list"

command -v sudo >> /dev/null && usesudo="sudo -E" || usesudo=""

export _ERAAGENT_PEER_CERT_PASSWORD="$eraa_peer_cert_pwd"

echo
echo Running installer script $local_installer
echo

$usesudo "$local_installer"\
   --skip-license \
   --hostname "$eraa_server_hostname"\
   --port "$eraa_server_port"\
   --cert-path "$local_cert_path"\
   --cert-password "env:_ERAAGENT_PEER_CERT_PASSWORD"\
   --cert-password-is-base64\
   --initial-static-group "$eraa_initial_sg_token"\
   $(test -n "$local_ca_path" && echo --cert-auth-path "$local_ca_path")\
   $(test -n "$eraa_product_uuid" && echo --product-guid "$eraa_product_uuid")\
   $additional_params
