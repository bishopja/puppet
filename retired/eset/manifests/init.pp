#Creates eset class
#Tests for the existence of the ERAAgent and installs, if it's not there

class eset {
#file { '/etc/init.d/eraagent' :
#    ensure  => 'present',
#    content => file('eset/eraagent'),
#    mode    => '0755',
#}

service { 'eraagent' :
   enable => false,
   ensure => 'stopped',
}
service { 'esets' :
    enable => false,
    ensure => 'stopped',
    }
file { '/etc/init.d/eraagent':
    ensure => 'absent',
    }
file { '/etc/init.d/esets':
    ensure => 'absent',
    }
file { '/etc/opt/eset':
    ensure => 'absent',
    force => true,
    }
file { '/opt/eset':
    ensure => 'absent',
    force => true,
    }
    exec { 'killpid':
       command => "pkill ERAAgent;pkill ERAAgent; pkill ERAAgent; rm -f /var/run/eraagent.pid", 
       path   => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:",
       onlyif => '/usr/bin/pgrep ERAAgent',
                }
}
