##configures cylance in syslog to /var/log/cylance.log and sets rotation policy
 
class cylance::logs{
if $facts['cylog'] == 'notinstalled' {
file {"/etc/logrotate.d/cylance":
    ensure => present,
    owner => root,
    group => wheel,
    mode => '644',
    content => file('cylance/cylance')
    }

}#if 1
}#class

