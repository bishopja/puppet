#==Certs class.  Installs *.ninds.nih.gov certs on webservers

class certs{
	file { '/etc/httpd/certificates' :
		ensure => directory,
	}
	file { 'certificate':
		ensure => present,
		owner  => 'root',
		group => 'apache',
		mode => '0660',
		path => '/etc/httpd/certificates/ninds.nih.gov.cer',
		content => file('certs/ninds.nih.gov.cer'),
	}
	file { 'key':
		ensure => present,
		owner => 'root',
		group => 'apache',
		mode => '0660',
		path => '/etc/httpd/certificates/ninds.nih.gov.key',
		content => file('certs/ninds.nih.gov.key'),
	}
	file { 'chain':
		ensure => present,
		owner => 'root',
		group => 'apache',
		mode => '0660',
		path => '/etc/httpd/certificates/nindsChain.cer',
		content => file ('certs/nindsChain.cer'),
	}
}
