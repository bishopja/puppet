class restart{
file {'/usr/local/bin/restart.sh':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0655',
    content => file('restart/restart.sh'),
  }
exec { 'restart':
   command =>'/usr/local/bin/restart.sh',
   require =>File['/usr/local/bin/restart.sh'],
}
}
