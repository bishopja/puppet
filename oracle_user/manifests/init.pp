class oracle_user{



file { '/usr/local/bin/check_pwdage.rb' :
    ensure  => 'present',
    content => file('oracle_user/check_pwdage.rb'),
    mode    => '0655',
}
cron { 'check_pwdage' :
    ensure => 'present',
    command => '/usr/local/bin/check_pwdage.rb',
    user => 'root',
    hour => 10,
    minute => 0,
	}
user { 'oracle' :
    password_min_age => "1",
    password_max_age => "180",
}
}
