#!/opt/puppetlabs/puppet/bin/ruby

require 'date'
yrs = {"Jan"=>"1", "Feb"=>"2", "Mar"=>"3", "Apr"=>"4", "May"=>"5", "Jun"=>"6", "Jul"=>"7", "Aug"=>"8", "Sep"=>"9", "Oct"=>"10", "Nov"=>"11", "Dec"=>"12"}

now=Date.today
last_password_change1=`/usr/bin/chage -l oracle|/bin/grep Last|/bin/awk -F ":" '{print $2}'`
last_password_change1=last_password_change1.strip
lpwdc_array=last_password_change1.split(" ")
lpwdc_array[1]=lpwdc_array[1].chomp(",")
lpwdc_array[0]=yrs[lpwdc_array[0]]
lpwdc=Date.new(lpwdc_array[2].to_i, lpwdc_array[0].to_i, lpwdc_array[1].to_i)
pwd_age=(now - lpwdc).to_i
hn=`/usr/local/bin/facter hostname`
hn.chomp!
case hn
when /tbi/
        recipients="nindsdirserveradmins@ninds.nih.gov scott.mitchell@nih.gov noah.bende@nih.gov"
else
        recipients="nindsdirserveradmins@ninds.nih.gov yavatka@ninds.nih.gov essiamso@ninds.nih.gov"
end
if pwd_age > 175
        `/bin/echo "Please change password for user oracle on #{hn}.  It is #{pwd_age} days old." |/bin/mail -s "Oracle account password change cron notification" #{recipients}`
end