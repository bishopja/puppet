#==Class nessusagent::install
#installs the nessus agent
class nessusagent::install {

  package { 'NessusAgent' :
    ensure => 'installed',
    }
  service { 'nessusagent' :
    ensure => 'running',
    enable => true,
    }
}
