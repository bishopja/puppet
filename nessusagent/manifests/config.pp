#==Class nessusagent::config
#configures (links) nessusagent


class nessusagent::config {
unless 'ocio' in $facts['na_link']{
  $groups = "NINDS"
  if 'nccih' in $facts['hostname']{
    $groups = "NCCIH"
  }
  exec { 'link_na':
    command => "/opt/nessus_agent/sbin/nessuscli agent link --key=754fc3a9c43f4557b5ec1a61d2d3c5c106b9a8a9f58e445ad4c2c7714b594d41 --host=isao-pa63-vl.ocio.nih.gov --port=443 --groups=$groups",
    notify => Service['nessusagent'],
    creates =>'/opt/nessus_agent/etc/nessus/nessusd.rules',
    }
  }
}
