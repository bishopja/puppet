#==Class bmshutdowns
## A simple class to copy a shutdown script and run it for the bimonthly shutdowns

class bmshutdowns{

file {'/usr/local/bin/bmshutdowns.sh':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0655',
    content => file('bmshutdowns/bmshutdowns.sh'),
  }

case $system_purpose{
  'puppetmaster':{
    cron { 'bmshutdown':
      command =>'/usr/local/bin/bmshutdowns.sh',
      hour => 6,
      minute => 1,
      weekday => 6,
      month => 12,
      monthday => 16,
      ensure => present,
      user => 'root',
    }
  }
 # 'backend':{
 #   cron { 'bmshutdown':
 #     command =>'/usr/local/bin/bmshutdowns.sh',
 #     hour => 7,
 #     minute => fqdn_rand(30),
 #     weekday => 6,
 #     month => 6,
 #     monthday => 17,
 #     ensure => present,
 #     user => 'root',
 #     }
 'backend':{
   cron { 'bmshutdown':
      command =>'/usr/local/bin/bmshutdowns.sh',
      hour => 7,
      minute => 0,
      weekday => 6,
      month => 12,
      monthday => 16,
      ensure => present,
      user => 'root',
      }

  }
  'frontend':{
    cron{ 'bmshutdown':
      command =>'/usr/local/bin/bmshutdowns.sh',
      hour => 9,
      minute => fqdn_rand(30),
      weekday => 6,
      month => 12,
      monthday => 16,
      ensure => present,
      user => 'root',
     }
  }
  'default': {}
  }
}
