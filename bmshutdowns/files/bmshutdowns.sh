#!/bin/bash


DOW=`date +%a`
DAY=`date +%d`
#[[ "$DAY" -gt 14 ] && [ "$DAY" -lt 22 ]] && [ "$DOW" = "Sat" ]
if [ "$DAY" -gt 14 ] && [ "$DAY" -lt 22 ] && [ "$DOW" = "Sat" ]; then
   mail -s "`hostname` shuttingdown now" jack.bishop@nih.gov
   /usr/bin/yum -y update
   /sbin/shutdown -r now
fi

