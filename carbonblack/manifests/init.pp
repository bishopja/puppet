#==Class carbonblack
#installs carbon black sensor client

class carbonblack{
        file{ '/usr/local/bin/CarbonBlackClientSetup-linux-v5.2.7.70316.rpm':
                ensure => 'present',
                content => file('carbonblack/CarbonBlackClientSetup-linux-v5.2.7.70316.rpm'),
        }
        file{ '/usr/local/bin/CarbonBlackClientSetup-linux-v5.2.7.70316.sh':
                ensure => 'present',
                content => file('carbonblack/CarbonBlackClientSetup-linux-v5.2.7.70316.sh'),
                mode =>'0770',
        }
        file{ '/usr/local/bin/sensorsettings.ini':
                ensure => 'present',
                content => file('carbonblack/sensorsettings.ini'),
        }
        exec { 'installcb':
                command => '/usr/local/bin/CarbonBlackClientSetup-linux-v5.2.7.70316.sh',
                creates => '/etc/init.d/cbdaemon',
                cwd => '/usr/local/bin',
        }
        service { 'cbdaemon':
                enable => true,
                ensure => 'running',
        }
}

