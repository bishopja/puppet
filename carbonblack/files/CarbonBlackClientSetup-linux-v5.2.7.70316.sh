#!/bin/bash
#
# Bootstraps the installation process after the user downloads the zip install package
#    Assumes running as su
#
#	1) Copies the sensor config file to the right location
#	2) Runs the RPM
#
if [ "$EUID" -ne 0 ]; then
    echo This script must be run as root
    exit 1
fi

RPM_NAME=CarbonBlackClientSetup-linux-v5.2.7.70316.rpm
LOGSDIR=/var/log/cbsensor
LOG_FILE=$LOGSDIR/install.log
SETTINGS_DST_PATH=/var/lib/cb
OLDLOGSDIR=/var/log/cb/sensor
PATH=$PATH:/sbin:/usr/sbin:/bin:/usr/bin
RUNNING_KERNEL=`uname -r`
KERNELBASE=`echo $RUNNING_KERNEL |awk '{split($0,a,"-"); print a[1]}'`
PLATFORM_RPM_DIR=/opt/cbsensor
PLATFORM_RPM=
if [ "$KERNELBASE" == "2.6.32" ]; then
    PLATFORM_RPM=$PLATFORM_RPM_DIR/setup-redhat6.rpm
elif [ "$KERNELBASE" == "3.10.0" ]; then
    PLATFORM_RPM=$PLATFORM_RPM_DIR/setup-redhat7.rpm
fi

function version_gt() { test "$(echo "$@" | tr " " "\n" | sort -V | head -n 1)" != "$1"; }

RPM=`which rpm`
if [ -z "$RPM" ]; then
    RPM=/usr/bin/rpm
fi

YUM=`which yum`
if [ -z "$YUM" ]; then
    YUM=/usr/bin/yum
fi

# Exit on error

set -e

#
# Figure out where we were run from
#
SCRIPT=$(readlink -f "$0")
INSTALLDIR=$(dirname "$SCRIPT")
DATE=$(date)

if [[ ! ( -d $LOGSDIR || -f $LOG_FILE ) ]]; then
    mkdir -p $LOGSDIR
    touch $LOG_FILE
fi

echo "############################################################################" | tee -a $LOG_FILE
echo "## $DATE" >> $LOG_FILE
echo "## Installing cbsensor from $INSTALLDIR" | tee -a $LOG_FILE

#
# Setup paths for files 
#
RPM_PATH=$INSTALLDIR/$RPM_NAME
SETTINGS_SRC_PATH=$INSTALLDIR/sensorsettings.ini

#
# Make sure the necessary files are present
#
if [[ ! -f $RPM_PATH ]]; then
	echo "## Error: RPM is not present" | tee -a $LOG_FILE
	exit -2
fi

if [[ ! ( -f $SETTINGS_SRC_PATH || -f $SETTINGS_DST_PATH/sensorsettings.ini ) ]]; then
	echo "## Error: config file is not present" | tee -a $LOG_FILE
	exit -2
fi

#
# Delete the old logs directory
#
if [[ -d $OLDLOGSDIR ]]; then
        rm -rf $OLDLOGSDIR
fi

#
# Copy the settings file
#
if [[ ! -d $SETTINGS_DST_PATH ]]; then
        mkdir -p $SETTINGS_DST_PATH
fi

if [[ -f $SETTINGS_SRC_PATH ]]; then
    cp -f $SETTINGS_SRC_PATH $SETTINGS_DST_PATH
    chmod 644 $SETTINGS_DST_PATH/sensorsettings.ini
fi

# Make sure the meta package is not there already
CB_RPM_UNIST=$($RPM -qa |grep cb-linux-sensor || true)

if [[ ! -z $CB_RPM_UNIST ]]; then
    echo "## Remove meta rpm" | tee -a $LOG_FILE
    ($RPM -e $CB_RPM_UNIST >> $LOG_FILE 2>&1)
    echo "############################################################################" | tee -a $LOG_FILE
fi

# Check to see if we have a duplicate package.  This would mean that a previous install
#  attempt failed for some reason.  Go ahead and delete the duplicate.
hasDups=$($RPM -qa|grep cbsensor|awk '{ a=1-a; if(!a) print $0 }')
if [ -n "$hasDups" ]; then
    dups=$($RPM -qa --queryformat '%{version} ' cbsensor 2>/dev/null)
    echo "## Duplicate sensors detected: $dups" | tee -a $LOG_FILE
    echo "## Attempting to remove $hasDups" | tee -a $LOG_FILE
    ($RPM -e --nodeps $hasDups >> $LOG_FILE 2>&1)
    echo "############################################################################" >> $LOG_FILE
fi

#
# Install the RPM
#
($RPM -i $RPM_PATH | tee -a $LOG_FILE)
if [ ${PIPESTATUS[0]} -ne 0 ]; then
    echo "install failed on meta RPM" | tee -a $LOG_FILE
    exit 1
fi

# Figure out the currently installed version and the new version to install.
oVersion=$($RPM -qa --queryformat '%{version}' cbsensor 2>/dev/null)
nVersion=$($RPM -qp --queryformat '%{version}' $UPGRADEDIR/$PLATFORM_RPM 2>/dev/null)

# The default operation is to install
op="install"

# If the new version is the same as the installed version we need a reinstall
if [ "$oVersion" == "$nVersion" ]; then
    op="reinstall"

# If the new version is older than the installed version we need a downgrade
elif version_gt "$oVersion" "$nVersion"; then
    op="downgrade"
fi

echo "## $op $oVersion -> $nVersion" | tee -a $LOG_FILE

# Call yum to do the required operation
if (test -f $YUM); then
    ($YUM $op -y $PLATFORM_RPM 2>&1 | tee -a $LOG_FILE)
else
    ($RPM -e cbsensor 2>&1 | tee -a $LOG_FILE)
    ($RPM -i $PLATFORM_RPM 2>&1 | tee -a $LOG_FILE)
fi

CB_RPM_UNIST=$($RPM -qa |grep cb-linux-sensor || true)
if [[ ! -z $CB_RPM_UNIST ]]; then
    echo "## Remove meta rpm" | tee -a $LOG_FILE
    ($RPM -e $CB_RPM_UNIST >> $LOG_FILE 2>&1)
fi
rm -f $PLATFORM_RPM_DIR/*.rpm >/dev/null 2>/dev/null
