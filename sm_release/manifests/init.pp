##sets the sticky OS subscription-managre release
# corresponds to the fact 'sm_release'

class sm_release{
if $facts['sm_release'] == 'Release not set'{
    case $facts['os']['release']['major']{
    '7':{
        exec { '/usr/sbin/subscription-manager release --set=7.4':}
    }
    '6':{
        exec { '/usr/sbin/subscription-manager release --set=6.9':}
    }
    default :{
        #exec { '/usr/sbin/subscription-manager release --set=6.9':}
        }
    }
}
}
