# == Class: samba
#
class samba(
) {

  file { '/etc/samba/smb.conf':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '644',
    content => template('samba/smb.conf.erb'),
  }
service { 'smbd' :
	ensure => stopped,
	enable => false,
}
}
